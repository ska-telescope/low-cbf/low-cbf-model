# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
""" Convert RAW format files to PCAP """
import argparse

from dpkt.pcap import UniversalReader

from ska_low_cbf_model import raw


def command_line_args() -> argparse.Namespace:
    """Configure command-line arguments & parse them for use."""
    parser = argparse.ArgumentParser(
        description="PCAP to RAW",
        epilog="Warning: Only compatible with SPS SPEAD protocol v2!",
    )
    parser.add_argument(
        "pcap", type=argparse.FileType("rb"), help="Input PCAP(NG) file"
    )
    parser.add_argument(
        "headers", type=argparse.FileType("wb"), help="Output RAW header file"
    )
    parser.add_argument(
        "data", type=argparse.FileType("wb"), help="Output RAW data file"
    )
    return parser.parse_args()


def main():
    """PCAP -> RAW Main Function"""
    args = command_line_args()

    pcap_reader = UniversalReader(args.pcap)
    raw.write_raw(args.headers, args.data, pcap_reader)


if __name__ == "__main__":
    main()
