# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
# All rights reserved

"""SPEAD Protocol definitions for LFAA (SPS) packets"""

import numpy as np

# 2048 dual pol samples x 2 pol x 2 bytes per sample = 8192 bytes of data
SAMPLES_PER_PACKET = 2048

# The SPEAD header is 72 bytes, consisting of 9 x (8 byte words)
# First word contains stuff set by the SPEAD protocol, remaining words are
# specific to LFAA SPEAD packets, with a 2 byte ID followed by 6 bytes of
# information.
lfaa_spead_header = np.dtype(
    [
        # First word : magic (0x53), version (0x04), itemPointerWidth (0x02),
        # headAddrWidth (0x06), reserved (0x0000), number of items (0x08)
        ("magic", "<u1"),  # SPEAD magic value, should be 0x53
        ("version", "<u1"),  # version should be 0x04
        ("item_pointer_width", "<u1"),  # 1 byte, should be 0x02
        ("heap_addr_width", "<u1"),  # 1 byte, should be 0x06
        ("w1_reserved", ">u2"),  # 2 byte reserved field, unused.
        ("N_items", ">u2"),  # 2 bytes, should be 0x08
        # Second word : heap counter, logical channel ID, packet counter
        ("heap_counter_id", ">u2"),  # SPEAD ID 0x8001
        ("logical_channel", ">u2"),  #
        ("packet_counter", ">u4"),  #
        # 3rd word : packet length
        ("packet_length_id", ">u2"),  # should be 0x8004
        (
            "packet_length_high",
            ">u2",
        ),  # Should be 8192, the number of bytes in the payload.
        ("packet_length_low", ">u4"),
        # 4th word : sync time
        ("sync_time_id", ">u2"),  # should be 0x9027
        ("sync_time_high", ">u2"),  # sync time in seconds from the unix epoch
        ("sync_time_low", ">u4"),
        # 5th word : timestamp
        ("timestamp_id", ">u2"),  # SPEAD timestamp, should be 0x9600
        ("timestamp_high", ">u2"),  # time in ns after sync_time
        ("timestamp_low", ">u4"),
        # 6th word : center frequency
        (
            "center_frequency_id",
            ">u2",
        ),  # SPEAD center frequency ID, should be 0x9011
        ("center_frequency_high", ">u2"),  # center frequency in Hz
        ("center_frequency_low", ">u4"),
        # 7th word : channel info
        (
            "csp_channel_info_id",
            ">u2",
        ),  # SPEAD channel info ID, should be 0xB000
        ("csp_channel_unused", ">u2"),  # 2 bytes unused in channel info
        ("csp_channel_beam_id", ">u2"),  # 2 bytes beam id
        ("csp_channel_frequency_id", ">u2"),  # 2 byte frequency ID
        # 8th word : antenna info
        ("csp_antenna_info_id", ">u2"),  # SPEAD antenna info, should be 0xB001
        ("csp_substation_id", ">u1"),  #
        ("csp_subarray_id", ">u1"),  #
        ("csp_station_id", ">u2"),  # station id
        ("csp_nof_contributing_antennas", ">u2"),
        # 9th word : sample offset
        (
            "sample_offset_id",
            ">u2",
        ),  # sample offset, should be 0x3300. Sample offset is not used.
        ("sample_offset_high", ">u2"),  # (unused), should be 0x0
        ("sample_offset_low", ">u4"),
    ]
)

complex_int8 = np.dtype([("re", np.int8), ("im", np.int8)])


def complex64_from_complex_int8(samples: np.ndarray) -> np.ndarray:
    """
    Convert complex_int8 to np.complex64
    """
    assert samples.dtype == complex_int8
    samples_c64 = np.zeros(samples.shape, dtype=np.complex64)
    samples_c64.real = samples["re"]
    samples_c64.imag = samples["im"]
    return samples_c64
