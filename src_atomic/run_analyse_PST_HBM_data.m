%% Analyse data downloaded from the HBM for PST processing
fname0 = 'run1/alveo_HBM_capture/hbm0_dump.dat';
fname1 = 'run1/alveo_HBM_capture/hbm1_dump.dat';
CT = 0;  % 0 = first corner turn
Nchannels = 8;
CTLength = 27;  % 27 LFAA packets = 60 ms corner turn

PlotBuffer = 1;  % Just plot the first buffer captured.

[doutPol0, doutPol1] = analyse_PST_HBM_data(fname0,CT,Nchannels,CTLength);

% Plot the data stream for each channel
%  doutPol0(buffer,channel,packet,sample)
%  doutPol1(buffer,channel,packet,sample)

for c = 1:Nchannels
    figure(c);
    clf;
    hold on;
    grid on;
    pStream0 = zeros(27*2048,1);
    pStream1 = zeros(27*2048,1);
    for p = 1:27
        pStream0((p-1)*2048+1 : (p-1)*2048 + 2048) = doutPol0(PlotBuffer,c,p,:);
        pStream1((p-1)*2048+1 : (p-1)*2048 + 2048) = doutPol1(PlotBuffer,c,p,:);
    end
    plot(real(pStream0),'r.-');
    plot(imag(pStream0),'g.-');
    plot(real(pStream1),'b.-');
    plot(imag(pStream1),'c.-');
    title(['LFAA input data, Channel = ' num2str(c)]);
end

CT = 1;  % 1 = second corner turn = output of the filterbanks
[FBPol0, FBPol1] = analyse_PST_HBM_data(fname1,CT,Nchannels,CTLength);

for c = 1:Nchannels
    figure(c + Nchannels);
    clf;
    hold on;
    grid on;
    FBstream0 = zeros((27/3)*32 * 216,1);
    FBstream1 = zeros((27/3)*32 * 216,1);
    for p = 1:((27/3)*32)
        FBstream0((p-1)*216+1 : (p-1)*216 + 216) = FBPol0(PlotBuffer,c,p,:);
        FBstream1((p-1)*216+1 : (p-1)*216 + 216) = FBPol1(PlotBuffer,c,p,:);
    end
    plot(real(FBstream0),'r.-');
    plot(imag(FBstream0),'g.-');
    plot(real(FBstream1),'b.-');
    plot(imag(FBstream1),'c.-');
    title(['Filterbank output data, channel = ' num2str(c)]);
end


