% Generate 512 station locations on a grid
for gx = 1:16
    for gy = 1:32
        disp(['    [' num2str((gx-1)*500) ',' num2str((gy-1)*500) '],']);
    end
end

s1 = ['  '];
for n =1:512
    s1 = [s1 ',' num2str(n)];
end

disp(s1)

s2 = [' '];
for n = 1:64
    s2 = [s2 ',' num2str(n)]; 
end

s3 = [' '];
for n = 1:64
    s3 = [s3 ', 0.95']; 
end

% Random station locations
for s1 = 1:64
    px = round(rand(1)*15000);
    py = round(rand(1)*15000);
    disp(['    [' num2str(px) ',' num2str(py) '],']);
end
